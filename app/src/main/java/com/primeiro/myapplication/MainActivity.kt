package com.primeiro.myapplication

import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val countries = listOf(
        "Czech",
        "Denmark",
        "Finland",
        "France",
        "Germany",
        "Ireland",
        "Italy",
        "Netherlands",
        "Portugal",
        "Sweden",
        "United Kingdom"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        for(country: String in countries){
            var country_entry = layoutInflater.inflate(R.layout.country_entry,null,false)
            var country_name = country_entry.findViewById<TextView>(R.id.country_name_field)
            var country_image = country_entry.findViewById<ImageView>(R.id.country_flag_image)

            country_name.text = country
            var imageName = country.toLowerCase().replace(" ","") + "_flag"
            var imageId = resources.getIdentifier(imageName, "drawable", packageName)
            country_image.setImageResource(imageId)
            country_image.setOnClickListener {
                showCountryInfo(country)
            }

            layout_countries.addView(country_entry)
        }

    }

    fun showCountryInfo(countryName: String){
        val resName = countryName.toLowerCase().replace(" ","") + "_info"
        val resId = resources.getIdentifier(resName, "raw", packageName)
        val text = resources.openRawResource(resId).bufferedReader().readText()

        val audName = countryName.toLowerCase().replace(" ","") + "_anthem"
        val audId = resources.getIdentifier(audName, "raw", packageName)
        val mp = MediaPlayer.create(this, audId)
        mp.start()

        val builder =  AlertDialog.Builder(this)
        builder.setTitle("Info about $countryName")
        builder.setMessage(text)
        builder.setPositiveButton("OK") { _, _ ->
            mp.stop()
        }
        var dialog = builder.create()
        dialog.show()

    }

}
